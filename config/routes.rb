Rails.application.routes.draw do
  devise_for :users  

  namespace :api, defaults: {format: :json} do
    devise_scope :user do
      resource :session, only: [:create, :destroy]
    end
    
  end

  root :to => "home#index"

  get '/dashboard' => 'templates#index'
  get '/task_lists/:id' => 'templates#index'
  get '/templates/:path.html' => 'templates#template', :constraints => { :path => /.+/  }

  namespace :api do
    scope :app, module: 'app' do
      get  :api1  # get list task
      get  :api2
      post :api3
      post :api4
      post :api5
      post :api6
    end

  end
end
