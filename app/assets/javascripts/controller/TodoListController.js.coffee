angular.module('app').controller "TodoListController", ($scope, $routeParams, $location, ApiHelper) ->
  $scope.sortMethod = 'priority'
  $scope.sortableEnabled = true
  $scope.taskDescription = ""
  $scope.total = 0
  $scope.lists = []

  $scope.returnPage = () ->
    window.location.href= "/"
    return
  $scope.deleteTask = (item) ->
    if item == null
      return
    ApiHelper.post('/api/app/api6',{ id: item.id}).then (res) ->
      console.log res.status
      if res.status == 1
        $scope.load($routeParams.list_id)
      else
        console.log("error")
    return
  $scope.addTask = () ->
    if $routeParams.list_id == null
      return
    ApiHelper.post('/api/app/api5',{title: $scope.taskDescription, id: $routeParams.list_id}).then (res) ->
      console.log res.status
      if res.status == 1
        $scope.load($routeParams.list_id)
      else
        console.log("error")
    return
  $scope.load = (id) ->
    if(!id)
      return
    ApiHelper.get('/api/app/api2',{id: id}).then (res) ->
      console.log res.status
      if res.status == 1
        $scope.lists = res.data.rows
        $scope.total = res.data.total
      else
        console.log("error")
  $scope.init = () ->
    if $routeParams.list_id
      console.log("$routeParams.list_id")
      $scope.load($routeParams.list_id)
    return





  serverErrorHandler = ->
    alert("There was a server error, please reload the page and try again.")