module ApplicationHelper
	def active_if_current(path)
		'active' if current_page?(path)
	end
	def render_success(object = nil, message = nil, response_type = 'json')
		hash = { status: 1 }
		hash[:data] = object
		hash[:message] = message unless message.nil?
		if response_type == 'json'
			render json: JSON.pretty_generate(JSON.parse(hash.to_json))
		elsif response_type == 'html'
			render text: JSON.pretty_generate(JSON.parse(hash.to_json))
		end
	end

	def render_failed(reason_number, message, response_type = 'json')
		hash = { status: reason_number, message: message }
		if response_type == 'json'
			render json: JSON.pretty_generate(JSON.parse(hash.to_json))
		elsif response_type == 'html'
			render text: JSON.pretty_generate(JSON.parse(hash.to_json))
		end
	end

	def render_500(exception = nil)
		message = "lỗi 500"
		status = 0
		if exception
			logger.info "Rendering 500 with exception: #{exception.message} \n#{exception.backtrace.join("\n")}"
			if exception.message == 'ActionController::InvalidAuthenticityToken'
				status = 105
			end
			unless Rails.env == 'production'
				message = "#{exception.message}\n#{exception.backtrace.join("\n")}"
			end
		end

		render_failed(status, message)
	end

	def render_failed_by_no_login(message = 'No login', response_type = 'json')
		render_failed(102, message, response_type = 'json')
	end

	def render_failed_by_missing_params(message = 'Missing params', response_type = 'json')
		render_failed(100, message, response_type = 'json')
	end

	def render_failed_invalid_params(message = 'Invalid params', response_type = 'json')
		render_failed(101, message, response_type = 'json')
	end

	def render_failed_by_required_operator(message = 'Error required operator', response_type = 'json')
		render_failed(103, message, response_type = 'json')
	end

	def render_failed_by_permission(message = 'No permission', response_type = 'json')
		render_failed(104, message, response_type = 'json')
	end

	def render_success_image_link(id = nil, link = nil, dimensions = nil, response_type = 'json')
		hash = { status: 1 }
		hash[:id] = id
		hash[:link] = link
		hash[:dimensions] = dimensions
		if response_type == 'json'
			render json: JSON.pretty_generate(JSON.parse(hash.to_json))
		elsif response_type == 'html'
			render text: JSON.pretty_generate(JSON.parse(hash.to_json))
		end
	end
end
