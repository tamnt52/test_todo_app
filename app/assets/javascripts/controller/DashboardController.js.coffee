angular.module('app').controller "DashboardController", ($scope, $routeParams, $location, ApiHelper) ->
  $scope.total = 0
  $scope.lists = []
  $scope.deleteList = (item, index) ->
    ApiHelper.post('/api/app/api4',{id: item.id}).then (res) ->
      console.log res
      if res.status == 1
        $scope.load()
      else
        console.log("error")
    return
  $scope.createList = (title) ->
    ApiHelper.post('/api/app/api3',{title: title}).then (res) ->
      console.log res
      if res.status == 1
        $scope.load()
      else
        console.log("error")
    return
  $scope.load = () ->
    ApiHelper.get('/api/app/api1',{}).then (res) ->
      console.log res.status
      if res.status == 1
        $scope.lists = res.data.rows
        $scope.total = res.data.total
      else
        console.log("error")
  $scope.init = () ->
    $scope.load()
    return





	serverErrorHandler = ->
       alert("There was a server error, please reload the page and try again.")