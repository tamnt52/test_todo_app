angular.module('app').factory "ApiHelper", ($http, $rootScope) ->
  console.log("use ApiHelper factory")
  factory_api = {}
  factory_api.get_authenticity_token = ()->
    $auth = $('meta').filter('[name=csrf-token]')
    token = if $auth.length > 0 then $auth.attr('content') else null
    return token
  factory_api.post = (url,params={}) ->
    $rootScope.$emit '$beginDataLoading'
    defer = $.Deferred()
    params.authenticity_token = factory_api.get_authenticity_token()
    $http.post(url,params).success((data) ->
      $rootScope.$emit '$closeDataLoading'
      if data.status == 105
        alert($translate.instant('sentence.page_has_expired'))
        window.location.reload()
      else
        defer.resolve(data)
      return
    ).error((data, status)->
      if status == 0
        return
      switch status
        when 404
          message = "Not Found URL : " + url
        else
          message = data
      $rootScope.$emit '$closeDataLoading'
      defer.resolve({status: status, message: message})
    )
    defer.promise();
  factory_api.get = (url,params={}) ->
    $rootScope.$emit '$beginDataLoading'
    defer = $.Deferred()
    $http.get(url,{params: params}).success((data) ->
      $rootScope.$emit '$closeDataLoading'
      defer.resolve(data)
      return
    ).error((data, status)->
      if status == 0
        return
      switch status
        when 404
          message = "Not Found URL : " + url
        else
          message = data
      $rootScope.$emit '$closeDataLoading'
      defer.resolve({status: status, message: message})
    )
    defer.promise();
  factory_api