class ApiController < ApplicationController
  protect_from_forgery with: :exception
  respond_to :json
  layout false  # not use layout
  skip_before_action :verify_authenticity_token, if: :skip_authenticity_token?
  protected
  def skip_authenticity_token?
    Rails.env == 'development'
  end
end
