class Api::AppController < ApiController
  include ApplicationHelper
  def api1
    render_failed_by_no_login and return unless user_signed_in?
    result = []
    result = TaskList.where(:owner_id => current_user.id)
    render_success(rows: result, total: result.count)
  end

  def api2
    render_failed_by_no_login and return unless user_signed_in?
    render_failed_by_missing_params('Missing params id') and return unless params[:id].present?
    result = []
    result = TaskList.find_by(:id => params[:id]).tasks
    render_success(rows: result, total: result.count)
  end

  def api3
    render_failed_by_no_login and return unless user_signed_in?
    render_failed_by_missing_params('Missing params title') and return unless params[:title].present?
    rs = TaskList.new(:owner => current_user, :name => params[:title])
    rs.save!
    render_success rs
  end

  def api4
    render_failed_by_no_login and return unless user_signed_in?
    render_failed_by_missing_params('Missing params id') and return unless params[:id].present?
    rs = TaskList.find_by(:id => params[:id])
    if rs.present?
      rs.destroy
    end
    render_success
  end

  def api5
    render_failed_by_no_login and return unless user_signed_in?
    render_failed_by_missing_params('Missing params title') and return unless params[:title].present?
    render_failed_by_missing_params('Missing params id') and return unless params[:id].present?
    rs = TaskList.find_by(:id => params[:id])
    tsk = Task.new(:description => params[:title], :list => rs)
    tsk.save!
    render_success tsk
  end

  def api6
    render_failed_by_no_login and return unless user_signed_in?
    render_failed_by_missing_params('Missing params id') and return unless params[:id].present?
    rs = Task.find_by(:id => params[:id])
    if rs.present?
      rs.destroy
    end
    render_success
  end



end